import { lazy } from "react";
import Layouts from "src/layout";
import ScreenRoutes from "./screenRoutes";
import { RoutePaths } from "./path";

interface IMenuItem {
  path: string;
  exact: boolean;
}

export const ListRoutes = (): any[] => {
  const screenRoutes = ScreenRoutes();
  return [
    {
      path: RoutePaths.Screen.Index,
      exact: true,
      component: (props: any) => <Layouts {...props} routes={screenRoutes} />,
      routes: screenRoutes,
    },
    {
      path: "",
      exact: false,
      component: lazy(() => import("src/pages/notFound")),
    },
  ];
};
//
