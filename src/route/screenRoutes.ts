import React, { lazy } from "react";
import { RoutePaths } from "./path";
import {
  faSortDown,
  faGlobe,
  faTh,
  faAppleAlt,
  faCircle,
  faSearch,
} from "@fortawesome/free-solid-svg-icons";
const ScreenRoutes = () => {
  return [
    {
      path: RoutePaths.Screen.Dashboard,
      name: "Dashboard",
      icon: faGlobe,
      component: lazy(() => import("src/pages/authenticated/dashboard")),
    },
    {
      // path: RoutePaths.Screen.Home.Index,
      name: "Home",
      icon: faAppleAlt,
      path: RoutePaths.Screen.Home.Index,
      routes: [
        {
          path: RoutePaths.Screen.Home.Index,
          name: "Home 1",
          component: lazy(() => import("src/pages/authenticated/home")),
        },
        {
          path: RoutePaths.Screen.Home.SubHome,
          name: "Home 2",
          component: lazy(() => import("src/pages/authenticated/home/home1")),
        },
        {
          path: RoutePaths.Screen.Home.SubHome2,
          name: "Home 3",
          component: lazy(() => import("src/pages/authenticated/home/home2")),
        },
      ],
    },
    {
      path: RoutePaths.Screen.Contact.Index,
      name: "Contact",
      icon: faTh,
      component: lazy(() => import("src/pages/authenticated/contact/index")),
      routes: [
        {
          path: RoutePaths.Screen.Contact.Index,
          name: "Contact",
          component: lazy(
            () => import("src/pages/authenticated/contact/index")
          ),
        },
        {
          path: RoutePaths.Screen.Contact.SubContact,
          name: "Contact",
          component: lazy(
            () => import("src/pages/authenticated/contact/subContact")
          ),
        },
      ],
    },
  ];
};
export default ScreenRoutes;
