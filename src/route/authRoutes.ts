import React, { lazy } from "react";
import { RoutePaths } from "./path";

export const AuthRoutes = () => {
  return [
    {
      path: RoutePaths.Auth.Index,
      // component: "src/pages/unAuthenticated/login",
      component: lazy(() => import("src/pages/unAuthenticated/login")),
    },
  ];
};
