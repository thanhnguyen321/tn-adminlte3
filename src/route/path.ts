export const RoutePaths = {
  Auth: {
    Index: "/",
    // Login: "/auth/login",
  },
  Screen: {
    Index: "/screen/Dashboard",
    Dashboard: "/screen/Dashboard",
    Home: {
      Index: "/screen/Home",
      SubHome: "/screen/Home1",
      SubHome2: "/screen/Home2",
    },
    Contact: {
      Index: "/screen/Contact",
      SubContact: "/screen/Contact1",
      SubContact2: "/screen/Conact2",
    },
  },
};
