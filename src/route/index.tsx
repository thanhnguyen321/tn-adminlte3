import React, { useRef, memo, useEffect, useState } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useRouteMatch,
  Redirect,
} from "react-router-dom";

import Login from "src/pages/unAuthenticated/login";
import Layouts from "src/layout";
import { ListRoutes } from "./listRoutes";
import Notfound from "src/pages/notFound";
import { AuthRoutes } from "./authRoutes";
import RouterGenerator from "src/core/routes/routerGenerator";
import { RoutePaths } from "src/route/path";
import ScreenRoutes from "./screenRoutes";
interface IMenuLink {
  lable: string | number;
  to: string;
  exact?: boolean;
}

// custom link
const MenuLink = ({ lable, to, exact }: IMenuLink) => {
  let match = useRouteMatch({ path: to, exact });
  return (
    <li className={match ? "active" : ""}>
      <Link to={to}>{lable}</Link>
    </li>
  );
};

const AppRoutesBased = () => {
  const guestRoutes = useRef(AuthRoutes()).current;
  const userRoutes = useRef(ListRoutes()).current;
  const [isLogin, setIsLogin] = useState(false);
  const isUser = localStorage.getItem("accessToken");

  console.log("item", localStorage.getItem("accessToken"));
  return (
    <>
      <Router>
        <Switch>
          <Route path="/" component={Login} exact />
          <Route path="/screen">
            {isUser === "true" ? <Layouts /> : <Redirect to="/" />}
          </Route>
          <Route component={Notfound} />
        </Switch>
      </Router>
    </>
  );
};
export const AppRoutes = memo(AppRoutesBased);
