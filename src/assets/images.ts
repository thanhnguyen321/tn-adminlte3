export const images = {
  loginBackground: require("./image/login.jpeg").default,
  notFound: require("./image/notfound01.jpeg").default,
  avatar: require("./image/avatar.jpeg").default,
};
