import React, { useMemo, lazy } from "react";
import {
  Redirect,
  BrowserRouter,
  Switch,
  Route,
  Link,
  useRouteMatch,
} from "react-router-dom";
import genneratorRoutes from "./genneratorRoutes";

interface RouterGeneratorProps {
  guestRoutes: any[];
  userRoutes: any[];
  isUser?: boolean;
  initialRouteName?: string;
  initialUserRouteName: string;
  initialGuestRouteName: string;
}

const RouterGenerator = ({
  guestRoutes,
  userRoutes,
  isUser = false,
  initialGuestRouteName,
  initialUserRouteName,
  initialRouteName = "/",
}: RouterGeneratorProps) => {
  const authenticated = () => {
    return (
      <>
        {userRoutes.map((item) => {
          <Switch>
            {item.routes ? (
              <>{item.routes.map((subItem) => {})}</>
            ) : (
              <Route to={item.path} component={item.component} />
            )}
          </Switch>;
        })}
      </>
    );
  };
  return (
    <BrowserRouter>
      {/* {isUser && (
        // <Switch>
        //   {guestRoutes.map((item, index) => {
        //     console.log("itemm", item);
        //     return (
        //       <Route
        //         path={item.path}
        //         // exact={index == 0 ? true : false}
        //         component={item.component}
        //         key={index}
        //       />
        //     );
        //   })}
        // </Switch>
        <Route>
          <Switch>
            {generatedGuestRedirectRoutes}
            {generatedUserRoutes}
            <Redirect to={initialUserRouteName} />
          </Switch>
        </Route>
      )}
      {!isUser && (
        <Route>
          <Switch>
            {generatedUserRedirectRoutes}
            {generatedGuestRoutes}
            <Redirect to={initialGuestRouteName} />
          </Switch>
        </Route>
      )}
      <Redirect to={initialRouteName} exact /> */}
    </BrowserRouter>
  );
};

export default RouterGenerator;

// interface IRouterGenerator {
//   guestRoutes: any; //guest: khach moi
//   userRoutes: any;
//   isUser: boolean;
//   initialRoutePath: string; //initial: ban đầu, path:đường dẫn; route:tuyến đường
//   initialUserRoutePath: string;
//   initialGuestRoutePath: string;
//   notFoundPath?: string;
// }

// interface IMenuLink {
//   lable: string | number;
//   to: string;
//   exact?: boolean;
// }

// const RouterGenerator = ({
//   guestRoutes,
//   userRoutes,
//   isUser,
//   initialRoutePath = "/",
//   initialUserRoutePath,
//   initialGuestRoutePath,
// }: IRouterGenerator) => {
//   const genneratorGuestRoutes = useMemo(() => {
//     return guestRoutes.map((routes) => {
//       return genneratorRoutes(routes);
//     });
//   }, [guestRoutes]);
//   const generatorGuestRedirectRoutes = useMemo(() => {
//     return guestRoutes.map((routes) => {
//       return genneratorRoutes({
//         ...routes,
//         exact: true,
//         redirect: initialUserRoutePath,
//       });
//     });
//   }, [guestRoutes, initialUserRoutePath]);

//   const generatorUserRoutes = useMemo(() => {
//     return userRoutes.map((routes) => {
//       return genneratorRoutes(routes);
//     });
//   }, [userRoutes]);

//   const generatorUserDedirectRoutes = useMemo(() => {
//     return userRoutes.map((routes) => {
//       return genneratorRoutes({
//         ...routes,
//         exact: true,
//         redirect: initialGuestRoutePath,
//       });
//     });
//   }, [userRoutes, initialGuestRoutePath]);
//   return (
//     <BrowserRouter>
//       {isUser && (
//         <Route>
//           <Switch>
//             {generatorGuestRedirectRoutes}
//             {generatorUserRoutes}
//           </Switch>
//         </Route>
//       )}
//       {!isUser && (
//         <Route>
//           <Switch>
//             {generatorUserDedirectRoutes}
//             {genneratorGuestRoutes}
//             <Redirect to={initialRoutePath} exact />
//           </Switch>
//         </Route>
//       )}
//     </BrowserRouter>
//   );
// };
// export default RouterGenerator;
