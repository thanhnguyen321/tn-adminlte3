import React, { useEffect, useState } from "react";
import "src/styles/component/menu.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faSortDown,
  faGlobe,
  faTh,
  faAppleAlt,
  faCircle,
  faSearch,
} from "@fortawesome/free-solid-svg-icons";
import { Link, Switch } from "react-router-dom";

const Menu = ({ data }: any) => {
  const [menuId, setMenuId] = useState<any>([]);
  const [subId, setSubId] = useState<string | number>();
  const styles = {
    displayNone: { display: "none" },
    displayBlock: { display: "block" },
    tranform: { transform: "translateY(-50%) rotate(180deg)" },
  };

  const handleClick = (item) => {
    let temp: any[] = [...menuId];
    if (item.routes) {
      if (menuId.indexOf(item.path) === -1) {
        temp.push(item.path);
      } else {
        temp.splice(temp.indexOf(item.path), 1);
      }
      setMenuId(temp);
    } else {
      setSubId(item.path);
    }
  };
  const handleClickSubItem = (item) => {
    setSubId(item);
  };

  return (
    <nav className="sidebar">
      <div className="text">
        <div className="logo">T</div>
        Admin LTE 3
      </div>
      <div className="info">
        <div className="img" />
        Thanh Nguyen
      </div>
      <div className="search">
        <input />
        <button onClick={() => {}}>
          <FontAwesomeIcon icon={faSearch} />
        </button>
      </div>
      <div className="menu">
        <ul>
          {data.map((item, index) => {
            return (
              <Switch key={index}>
                {item.routes ? (
                  <li>
                    <Link
                      href="#"
                      onClick={() => {
                        handleClick(item);
                      }}
                    >
                      <span className="sub_icon">
                        <FontAwesomeIcon icon={item.icon} />
                      </span>
                      {item.name}
                      <span
                        className="icon"
                        style={
                          menuId.indexOf(item.path) >= 0 ? styles.tranform : {}
                        }
                      >
                        <FontAwesomeIcon icon={faSortDown} />
                      </span>
                    </Link>
                    <ul
                      className="subMenu"
                      style={{
                        display:
                          menuId.indexOf(item.path) >= 0 ? "block" : "none",
                      }}
                    >
                      {item.routes.map((subItem) => {
                        return (
                          <li key={subItem.path}>
                            <Link
                              to={subItem.path}
                              className={subItem.path === subId ? "active" : ""}
                              onClick={() => handleClickSubItem(subItem.path)}
                            >
                              <span className="sub_icon">
                                <FontAwesomeIcon
                                  icon={faCircle}
                                  fontSize={14}
                                />
                              </span>
                              {subItem.name}
                            </Link>
                          </li>
                        );
                      })}
                    </ul>
                  </li>
                ) : (
                  <li>
                    <Link
                      // href={item.path}
                      className={item.path === subId ? "active" : ""}
                      onClick={() => handleClickSubItem(item.path)}
                    >
                      <span className="sub_icon">
                        <FontAwesomeIcon icon={item.icon} />
                      </span>
                      {item.name}
                    </Link>
                  </li>
                )}
              </Switch>
            );
          })}
        </ul>
      </div>
    </nav>
  );
};
export default Menu;
