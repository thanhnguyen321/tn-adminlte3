import React from "react";
import "src/styles/component/button.css";
interface IButton {
  children: any;
  border?: boolean;
  width?: number | string;
  height?: number | string;
  onClick?: () => void | undefined;
  style?: any;
  background?: string;
}
const Button = ({
  children,
  border = true,

  width = "auto",
  height = "auto",
  background = "#fff",
  onClick,
  style,
  ...props
}: IButton) => {
  const styles = border ? "" : "no-bd";
  return (
    <button
      {...props}
      style={{
        background,
        width,
        height,
        // fontSize: 24,
        // marginRight: 20,
      }}
      className={styles}
      onClick={onClick}
    >
      {children}
    </button>
  );
};
export default Button;
