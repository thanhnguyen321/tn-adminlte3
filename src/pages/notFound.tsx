import React from "react";
import { images } from "src/assets/images";
const NotFound = () => {
  return (
    <div>
      <img src={images.notFound} style={{ height: "100%", width: "100%" }} />
    </div>
  );
};
export default NotFound;
