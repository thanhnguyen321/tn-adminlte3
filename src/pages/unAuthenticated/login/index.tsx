import React from "react";
import { useHistory } from "react-router-dom";
import { RoutePaths } from "src/route/path";
import "src/styles/unAuthenticatedStyle/login.css";
const Login = ({ isUser }: any) => {
  const history = useHistory();
  const handleLogin = () => {
    // isUser(true);
    localStorage.setItem("accessToken", "true");

    // history.replace(RoutePaths.Screen.Dashboard);
    window.location.href = RoutePaths.Screen.Dashboard;
  };
  return (
    <div className="login_container" style={{}}>
      <div className="card">
        <div className="title">
          <p>Login</p>
        </div>

        <div className="account">
          <p>Tài khoản:</p>
          <input />
        </div>
        <div className="password">
          <p>Mật khẩu:</p>
          <input />
        </div>
        {/* <div className="checkbox">
          <input type={"checkbox"} />
          <span className="checkmark"></span>
        </div> */}

        <div className="bnt-login">
          <button onClick={() => handleLogin()}>Đăng nhập</button>
        </div>
      </div>
    </div>
  );
};
export default Login;
