import React, { useState } from "react";
import "src/styles/authenticatedStyle/layout.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faAlignJustify,
  faGlobe,
  faTh,
  faAppleAlt,
  faMessage,
  faSearch,
  faBell,
  faArrowsAlt,
  faThLarge,
} from "@fortawesome/free-solid-svg-icons";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useRouteMatch,
  Redirect,
} from "react-router-dom";
import { images } from "src/assets/images";
import Menu from "src/component/menu";
import Button from "src/component/button";
import UserRoutes from "src/route/screenRoutes";
import Home from "src/pages/authenticated/home";
import Dashboard from "src/pages/authenticated/dashboard";
import Contact from "src/pages/authenticated/contact";
import { RoutePaths } from "src/route/path";

const LayOuts = () => {
  const [showMenu, setShowMenu] = useState(true);
  const userRoutes = UserRoutes();
  const data = [
    {
      key: 1,
      title: "Home",
      icon: faGlobe,
      subData: [
        { id: 4, name: "subHome 1" },
        { id: 2, name: "subHome 2" },
        { id: 3, name: "subHome 3" },
      ],
    },
    { key: 5, title: "About", icon: faTh },
    {
      key: 6,
      title: "Contact",
      icon: faAppleAlt,
      subData: [
        { id: 7, name: "subContact 1" },
        { id: 8, name: "subContact 2" },
        { id: 9, name: "subContact 3" },
      ],
    },
  ];

  const dataButton = [
    { id: 1, name: faSearch },
    { id: 2, name: faMessage },
    { id: 3, name: faBell },
    { id: 4, name: faArrowsAlt },
    { id: 5, name: faThLarge },
  ];

  const handleRightButton = (id) => {
    console.log("id", id);
  };

  const handleSubItem = (item) => {
    item.routes.map((subItem) => {
      console.log("subItem.component", subItem.component);
      return <Route path={subItem.path} component={subItem.component} />;
    });
  };

  return (
    <div className="container">
      <div
        className={`sidebars ${
          showMenu ? "left-menu-false" : "left-menu-true"
        }`}
      >
        <Menu data={userRoutes} />
      </div>

      <div className="content">
        <div className="head">
          <div className="hear-left">
            <Button onClick={() => setShowMenu(!showMenu)} border={false}>
              <FontAwesomeIcon icon={faAlignJustify} />
            </Button>
            <Button onClick={() => {}} border={false}>
              Home
            </Button>
            <Button onClick={() => {}} border={false}>
              Contact
            </Button>
          </div>
          <div className="hear-right">
            {dataButton.map((item) => {
              return (
                <Button
                  border={false}
                  onClick={() => handleRightButton(item.id)}
                >
                  <FontAwesomeIcon icon={item.name} />
                </Button>
              );
            })}
          </div>
        </div>
        <div className="body">
          <ul>
            {userRoutes.map((item) => {
              if (item.routes) {
                return (
                  <>
                    <div>abc</div>
                    {item.routes?.map((subitem) => {
                      return (
                        <li key={item.path}>
                          <Link to={item.path}>{item.name}</Link>
                        </li>
                      );
                    })}
                  </>
                );
              } else {
                return (
                  <li key={item.path}>
                    <Link to={item.path}>{item.name}</Link>
                  </li>
                );
              }
            })}
          </ul>
          <Switch>
            {/* <Route path={RoutePaths.Screen.Dashboard} component={Dashboard} />
            <Route path="/screen/Home" component={Home} />
            <Route path="/screen/Contact" component={Contact} /> */}
            {userRoutes.map((item) => {
              if (item.routes) {
                return (
                  <>
                    {item.routes.map((subItem) => {
                      console.log("subItem.component", subItem.component);
                      return (
                        <Route
                          path={subItem.path}
                          component={subItem.component}
                        />
                      );
                    })}
                  </>
                );
              } else {
                console.log("item.component", item.component);
                return <Route path={item.path} component={item.component} />;
              }
            })}
          </Switch>
        </div>
      </div>
    </div>
  );
};

export default LayOuts;
