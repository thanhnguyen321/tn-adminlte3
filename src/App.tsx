import React, { Suspense } from "react";

import { AppRoutes } from "src/route";
import { Provider } from "react-redux";
import { Store } from "src/redux/store";

function App() {
  return (
    <Provider store={Store}>
      <Suspense
        fallback={
          <div
            className="d-flex align-items-center justify-content-center"
            style={{ width: "100vw", height: "100vh" }}
          >
            Đang loading...
          </div>
        }
      >
        <AppRoutes />
      </Suspense>
    </Provider>
  );
}

export default App;
