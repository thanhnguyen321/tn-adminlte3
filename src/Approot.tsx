import React, { useContext, createContext, useState, Suspense } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect,
  useHistory,
  useLocation,
} from "react-router-dom";

const AppRoot = () => {
  return (
    <div>
      <Suspense
        fallback={
          <div
            className="d-flex align-items-center justify-content-center"
            style={{ width: "100vw", height: "100vh" }}
          >
            Đang loading...
          </div>
        }
      >
        {/* <AppRoot /> */}
      </Suspense>
    </div>
  );
};

export default AppRoot;
