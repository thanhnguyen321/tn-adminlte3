import { ofType } from "redux-observable";
export const pigEpic = (action$) =>
  action$.ofType("PING").delay(1000).mapTo({ type: "PING" });
// export const pingEpic = (action$) =>
//   action$.pipe(
//     ofType("PING")
//     .delay(1000) // Asynchronously wait 1000ms then continue
//     .mapTo({ type: "PONG" })
//   );
