import { combineReducers } from "redux";
import authModule from "src/redux/reducers/authModule";

export const rootReducer = combineReducers({
  auth: authModule,
});
