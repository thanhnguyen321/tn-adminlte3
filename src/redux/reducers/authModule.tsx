const pigReducer = (state = { isPing: false }, action) => {
  switch (action.type) {
    case "PING":
      return { isPing: true };
    case "PONG":
      return { isPing: false };
    default:
      return state;
  }
};
export default pigReducer;
