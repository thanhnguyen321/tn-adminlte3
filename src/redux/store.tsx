import { createStore, applyMiddleware, compose } from "redux";
import { createEpicMiddleware } from "redux-observable";
import { pigEpic } from "./epics";
import pigReducer from "./reducers/authModule";

const epicMiddleware = createEpicMiddleware();

export const Store = createStore(
  pigReducer,
  compose(applyMiddleware(epicMiddleware))
);
